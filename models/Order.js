const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},

	purchasedOn: {
			type: Date,
			default: new Date()
	},

	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required"]
	},
	
	quantity : {
		type: Number,
		required: [true, "Quantity is required"]
	},

	productName: {
		type: String,
		required: [true, "Product Name is required"]
	}
})


module.exports = mongoose.model("Order", orderSchema);