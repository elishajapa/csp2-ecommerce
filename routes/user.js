const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// Route for checking if the user's email already exists 
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authenticating a user
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Create Admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	userController.createAdmin(user, req.params).then(resultFromController => res.send(resultFromController));
	console.log(req.params);
})

// GET All Users 
router.get("/allusers", (req, res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
})


module.exports = router;