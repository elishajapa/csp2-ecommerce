const orderController = require("../controllers/order");
const express = require("express");
const router = express.Router();
const auth = require("../auth");

// Creating new order
router.post("/checkout", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	orderController.addOrder(user, req.body).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a single order
router.get("/myOrders", auth.verify,  (req, res) => {
	
	const user = auth.decode(req.headers.authorization).id
	orderController.getOne(user).then(resultFromController => res.send(resultFromController));
})


// Route for retrieving all orders
router.get("/all", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	orderController.getAllOrders(user).then(resultFromController => res.send(resultFromController));
}) 


module.exports = router;