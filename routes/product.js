const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");



// Creating new product
router.post("/", auth.verify, (req, res) => {
			const data =  auth.decode(req.headers.authorization)
			productController.addProduct(data, req.body).then(resultFromController => res.send(resultFromController));

		});

// Route for retrieving all products
router.get("/products", (req, res) => {
	productController.getAllproducts().then(resultFromController => res.send(resultFromController));
})

// Routes for retrieveing all active products
router.get("/active", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getOne(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	productController.updateProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// Archive product
router.put("/:productId/archive", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	productController.archiveProduct(req.params.productId, user).then(resultFromController => res.send(resultFromController));
})
module.exports = router;