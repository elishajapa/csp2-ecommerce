const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.addOrder = async (user, reqBody) => {
	if(user.isAdmin === false){
		let userName = await User.findOne({_id: user.id}).then(result => {
			return result.firstName + " " + result.lastName;
		});

		let productName = await Product.findOne({name: reqBody.name}).then(result => {
			return result.name;
		}); 

		let userID = await User.findOne({_id: user.id}).then(result => {
			return result.id;
		});

		let price = await Product.findOne({name: reqBody.name}).then(result => {
			return result.price;
		});

		let newOrder = new Order({
			userId: userID,
			quantity: reqBody.quantity,
			productName: productName,
			totalAmount: price*reqBody.quantity
		});
		
		User.findOne({_id: user.id}).then(result => {
			let orderSummary = {
				orderId: newOrder.id,
				product: newOrder.productName,
				totalAmount: newOrder.totalAmount
			}

			result.products.push(orderSummary);
			return result.save().then((result, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			})
		})

		Product.findOne({name: reqBody.name}).then(result => {
			let orderSummary = {
				userName: userName,
				quantity: reqBody.quantity,
			}

			result.ordered.push(orderSummary);
			return result.save().then((result, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			})
		})

		return newOrder.save().then((order, error) => {
			if(error){
				return "Failed. Please try again.";
			} else {
				return `Order successfully made. Thank you.`;
			}
		})
	} else {
		return "You don't have access"
	}
}

// Controlller for retrieving a single order
module.exports.getOne = (user) => {
	return User.findById(user).then(result => {
		return result.products;
	})
}

// Controlller for retrieveing  all orders
module.exports.getAllOrders = async (user) => {
	if(user.isAdmin === true){
	return Order.find({}).then(result => {
		return result;
	})
	}else{
		return false;
	}
}

