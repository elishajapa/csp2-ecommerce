const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Create a new course
module.exports.addProduct = async (user, reqBody) => {
	if(user.isAdmin){
			let newProduct = new Product ({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})

			return newProduct.save().then((result,error) => {
				if(error){
					return false;
				}
				else{
					return `Successfully created ${result.name} product with a price of ${result.price}`;
				}
			})
		}
		else{
			return("You have no access.")
		}
	}

// Controlller for retrieveing  all products
module.exports.getAllproducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

// retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

// Controlller for retrieving a single product
module.exports.getOne = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Controlller for updating a product  
module.exports.updateProduct = async (user, reqParams, reqBody) => {
	if(user.isAdmin){
		return Product.findById(reqParams.productId).then(result => {
			console.log(result);
			result.price = reqBody.price;
			result.description = reqBody.description;
			result.name = reqBody.name;

			return result.save().then((updatedResult, error) => {
				if(error){
					return "Failed to update";
				}
				else{
					return "You have successfully updated the product";
				}
			})
		})

		}else{
			return (`Sorry. Only the admin can update a product.`);
		}
	
	
	}

	// Archive Product
	module.exports.archiveProduct = async (reqParams, user) => {
		if(user.isAdmin){
			return Product.findByIdAndUpdate(reqParams, { $set: { isActive: false }}).then((result, error) => {
				if(error){
					return false;
				}
				else{
					return "Successfully archived product";
				}
			})
			
			}else{
				return (`You have no access`);
		}
		
	}