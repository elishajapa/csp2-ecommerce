const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if email already exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0){
			return "Email already exist.";
		}
		else{
			return "Sorry, I don't recognize this email.";
		}
	})
}

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return "Failed to register, please try again.";
		}
		else{
			return "Congratulations! You are now registered.";
		}
	})
}

// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}
			else{
				return false;
			}
		}
	})
}

// Set as Admin
module.exports.createAdmin = async (user, data) => {

		if(user.isAdmin){
			return User.findByIdAndUpdate({_id: data.userId}, {isAdmin: true}).then(result => {
				return result.save().then((updatedResult, err) => {
					if(err){
						return "Failed to update."
					}
					else{
						return updatedResult;
					}
				})
			})
		} else{
			return "You have no access."
		}
}

// GET All Users
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	})
}