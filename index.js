const express = require("express");
const mongoose = require("mongoose")
const cors = require("cors");
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");

const app = express();

// connect to mongoDB
mongoose.connect("mongodb+srv://dbelishajapa:2MmvHGadD5lLeaU1@wdc028-course-booking.qhurm.mongodb.net/ecommerce?retryWrites=true&w=majority",
{

	useNewUrlParser: true,
	useUnifiedTopology: true
}
);

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Routes for users - products - orders 
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);


app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});